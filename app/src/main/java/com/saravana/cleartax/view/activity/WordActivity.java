package com.saravana.cleartax.view.activity;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.saravana.cleartax.R;
import com.saravana.cleartax.databinding.ActivityHomeBinding;
import com.saravana.cleartax.model.datamodel.WordItem;
import com.saravana.cleartax.viewmodel.WordVM;

public class WordActivity extends BaseActivity {

    private ActivityHomeBinding binding;
    protected WordVM mViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            mViewModel = WordVM.getViewModel(this, WordActivity.class.getSimpleName());
            mViewModel.init();
        }

        binding.setWord(mViewModel);

        subscribeObserver();

    }

    private void subscribeObserver() {

        if (mViewModel == null) {
            return;
        }

        mViewModel.getAllWords().observe(this, wordItems -> {

            mViewModel.getWordForm().setWords(wordItems);

            if (wordItems == null || wordItems.isEmpty()) {
                resetValue();
                return;
            }

            WordItem wordItem = wordItems.get(0);

            mViewModel.getWordForm().setWordItem(wordItem);

            if (wordItem == null || TextUtils.isEmpty(wordItem.getWord())) {
                resetValue();
                return;
            }

            binding.edtWord.setText(wordItem.getWord());
            binding.edtWord.setSelection(binding.edtWord.getText().length());
            binding.txtCounter.setText(String.valueOf(wordItem.getWord().length()));
            binding.btnUndo.setEnabled(true);

        });


    }

    private void resetValue() {
        binding.edtWord.getText().clear();
        binding.txtCounter.setText("0");
        mViewModel.getWordForm().setWordItem(null);
        binding.btnUndo.setEnabled(false);
    }

    @Override
    protected void setContentBindingView() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        imgMenu = binding.actionHeader.imgMenu;
    }

    @Override
    public ViewDataBinding getDataBinding() {
        return binding;
    }

    @Override
    public WordVM getViewModel() {
        return mViewModel;
    }

}
