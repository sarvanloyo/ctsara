package com.saravana.cleartax.view.activity;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.databinding.ViewDataBinding;

import com.saravana.cleartax.R;
import com.saravana.cleartax.viewmodel.WordVM;

public abstract class BaseActivity extends AppCompatActivity {

    protected WordVM mAppVM;
    protected ImageView imgMenu;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentBindingView();

        addListener();

    }

    protected void addListener() {

        if (imgMenu != null) {

            imgMenu.setOnClickListener(v -> {

                if (imgMenu == null) {
                    return;
                }
                if (imgMenu.getTag() == null) {
                    return;
                }

                if (imgMenu.getTag().toString().equalsIgnoreCase(getString(R.string.menu_back_arrow))) {
                    onBackPressed();
                }

            });
        }

    }

    protected WordVM createAppVM(String key) {
        return WordVM.getViewModel(this, key);
    }

    protected abstract void setContentBindingView();

    public abstract ViewDataBinding getDataBinding();

    public abstract WordVM getViewModel();

}
