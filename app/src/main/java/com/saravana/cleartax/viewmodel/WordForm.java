package com.saravana.cleartax.viewmodel;

import android.text.TextUtils;

import androidx.databinding.BaseObservable;

import com.saravana.cleartax.model.datamodel.WordItem;

import java.util.List;

public class WordForm extends BaseObservable {

    private List<WordItem> words;
    private WordItem wordItem;
    private String word;

    public void setWords(List<WordItem> words) {
        this.words = words;
    }

    public void setWordItem(WordItem wordItem) {
        this.wordItem = wordItem;
    }

    public WordItem getWordItem() {
        return wordItem;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
        notifyChange();
    }

    public boolean isSameWord() {
        if (wordItem == null || TextUtils.isEmpty(word) || TextUtils.isEmpty(wordItem.getWord())) {
            return false;
        }
        return word.equals(wordItem.getWord());
    }

}
