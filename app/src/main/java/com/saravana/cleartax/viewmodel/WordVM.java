package com.saravana.cleartax.viewmodel;

import android.app.Application;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.saravana.cleartax.R;
import com.saravana.cleartax.application.CTApp;
import com.saravana.cleartax.model.datamodel.WordItem;
import com.saravana.cleartax.model.repository.WordRepo;

import java.util.List;

public class WordVM extends BaseViewModel implements View.OnClickListener {

    private WordForm wordForm;
    private View.OnFocusChangeListener onFocusWord;

    private LiveData<List<WordItem>> wordListLiveData;

    public WordVM(@NonNull Application application, String key) {
        super(application);
    }

    public static WordVM getViewModel(AppCompatActivity activity, String key) {
        WordVM.Factory factory = new WordVM.Factory(key);
        return ViewModelProviders.of(activity, factory).get(WordVM.class);
    }

    public static WordVM getViewModel(Fragment fragment, String key) {
        WordVM.Factory factory = new WordVM.Factory(key);
        return ViewModelProviders.of(fragment, factory).get(WordVM.class);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        private final String mKey;

        public Factory(String key) {
            this.mKey = key;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            return (T) new WordVM(CTApp.getAppContext(), mKey);
        }
    }

    @BindingAdapter("onFocus")
    public static void bindFocusChange(EditText editText, View.OnFocusChangeListener onFocusChangeListener) {
        if (editText.getOnFocusChangeListener() == null) {
            editText.setOnFocusChangeListener(onFocusChangeListener);
        }
    }

    public void init() {

        wordForm = new WordForm();

        onFocusWord = (v, hasFocus) -> {

            if (wordForm == null || TextUtils.isEmpty(wordForm.getWord()) || hasFocus) {
                return;
            }

            if (wordForm.isSameWord()) {
                return;
            }

            addWord(wordForm.getWord());

        };

    }

    public WordForm getWordForm() {
        return wordForm;
    }

    public View.OnFocusChangeListener getOnFocusWord() {
        return onFocusWord;
    }

    public LiveData<List<WordItem>> getAllWords() {

        if (wordListLiveData == null) {
            wordListLiveData = WordRepo.getInstance().readAllData();
        }

        return wordListLiveData;

    }

    public void addWord(String word) {

        if (TextUtils.isEmpty(word)) {
            return;
        }

        WordItem wordItem = new WordItem();
        wordItem.setWord(word);

        WordRepo.getInstance().addItem(wordItem);

    }

    public void removeWord(WordItem word) {

        if (word == null) {
            return;
        }

        WordRepo.getInstance().removeItem(word);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_undo:
                handleUndo();
                break;
        }

    }

    private void handleUndo() {

        if (wordForm == null) {
            return;
        }

        if (wordForm.isSameWord()) {

            removeWord(wordForm.getWordItem());

        } else if (wordForm.getWordItem() != null) {
            wordForm.setWord(wordForm.getWordItem().getWord());
        } else {
            wordForm.setWord("");
        }

    }

}
