package com.saravana.cleartax.application;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

public class CTApp extends Application {

    private static CTApp applicationContext;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = this;
        AppHandler.init();
    }

    public static CTApp getAppContext() {
        return applicationContext;
    }

    @Override
    public void onTerminate() {
        AppHandler.stopLifeCycleOwner();
        super.onTerminate();
    }

}
