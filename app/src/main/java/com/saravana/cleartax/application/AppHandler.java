package com.saravana.cleartax.application;

import com.saravana.cleartax.model.db.DBHelper;
import com.saravana.cleartax.model.repository.WordRepo;
import com.saravana.cleartax.utils.CTLifeCycleOwner;

public class AppHandler {

    private static CTLifeCycleOwner lifeCycleOwner = new CTLifeCycleOwner();

    public static void init() {

        DBHelper.getInstance(CTApp.getAppContext());

    }

    public static void stopLifeCycleOwner() {
        lifeCycleOwner.stopListening();
    }


}
