package com.saravana.cleartax.model.repository;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;

import com.saravana.cleartax.application.CTApp;
import com.saravana.cleartax.model.dao.WordDao;
import com.saravana.cleartax.model.datamodel.WordItem;
import com.saravana.cleartax.model.db.DBHelper;
import com.saravana.cleartax.utils.AppExecutors;

import java.util.List;

public class WordRepo extends BaseRepo {

    private static WordRepo mInstance;
    private WordDao wordDao;

    private WordRepo() {
        wordDao = DBHelper.getInstance(CTApp.getAppContext()).getDB().searchDao();
    }

    public static synchronized WordRepo getInstance() {
        if (mInstance == null) {
            synchronized (WordRepo.class) {
                if (mInstance == null) {
                    mInstance = new WordRepo();
                }
            }
        }
        return mInstance;
    }

    public LiveData<List<WordItem>> readAllData() {
        return wordDao.getAll();
    }

    public void addItem(WordItem word) {
        AppExecutors.getExecutors().diskIO().execute(() -> {
            try {
                if (word == null || TextUtils.isEmpty(word.getWord())) {
                    return;
                }
                wordDao.insertAll(word);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void removeItem(WordItem word) {
        AppExecutors.getExecutors().diskIO().execute(() -> {
            try {
                if (word == null || TextUtils.isEmpty(word.getWord())) {
                    return;
                }
                wordDao.delete(word);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
