package com.saravana.cleartax.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.saravana.cleartax.model.dao.WordDao;
import com.saravana.cleartax.model.datamodel.WordItem;

@Database(entities = {WordItem.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract WordDao searchDao();
}