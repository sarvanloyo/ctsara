package com.saravana.cleartax.model.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.saravana.cleartax.R;

public class DBHelper {

    private Context mContext;
    private static DBHelper mInstance;
    private AppDatabase appDatabase;

    private DBHelper(Context mCtx) {

        this.mContext = mCtx;

        appDatabase = Room.databaseBuilder(mContext, AppDatabase.class, mContext.getString(R.string.db_cleartax))
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }

                    @Override
                    public void onOpen(@NonNull SupportSQLiteDatabase db) {
                        super.onOpen(db);
                    }
                }).build();
    }

    public static synchronized DBHelper getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DBHelper(mCtx);
        }
        return mInstance;
    }

    public AppDatabase getDB() {
        return appDatabase;
    }


}
