package com.saravana.cleartax.model.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.saravana.cleartax.model.datamodel.WordItem;

import java.util.List;

@Dao
public interface WordDao {

    @Query("SELECT * FROM WordItem ORDER BY id DESC")
    LiveData<List<WordItem>> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(WordItem... words);

    @Delete
    void delete(WordItem word);

    @Query("DELETE FROM WordItem")
    void deleteAll();

}
